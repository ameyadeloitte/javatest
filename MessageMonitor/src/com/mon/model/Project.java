package com.mon.model;

import java.util.Calendar;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



@Entity
@Table(name = "PROJECT_REGISTRY")
@NamedQueries({
	@NamedQuery(name = "Project.findAll", query = "SELECT p FROM Project p"),
	@NamedQuery(name = "Project.findByName", query = "SELECT p FROM Project p WHERE p.projectId = :projectId")
})
public class Project {
	
	/* Customer ids are generated starting with 1 */
	@TableGenerator(name = "ProjectGenerator", table = "MSG_MON_ID_GENERATOR", pkColumnName = "GENERATOR_NAME", 
			valueColumnName = "GENERATOR_VALUE", pkColumnValue = "Project", initialValue = 1, allocationSize = 1)
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "ProjectGenerator")
    @Column(name = "PROJECT_ID")
    private int projectId;
    
    @Column(name = "PROJECT_DESC", nullable=false)
    private String projectDescription;
    
    @Column(name = "OWN_EMAIL_ID", nullable=false)
    private String ownerEmailId;
    
    @Column(name = "OWN_TELEPHONE", nullable=false)
    private String ownerTelephone;
    
    @Column(name = "CREATE_USER", length = 25, nullable=false)
    private String createUser;

    @Column(name = "CREATE_TIME")
	@Temporal(TemporalType.TIMESTAMP)
	protected Calendar createTimestamp;

    @Column(name = "UPDATE_USER", length = 25, nullable=false)
    private String updateUser;

    @Column(name = "UPDATE_TIME")
	@Temporal(TemporalType.TIMESTAMP)
	protected Calendar updateTimestamp;
    
	@PrePersist
	private void preInsert() {
		Calendar calendar = Calendar.getInstance();
		// Use the server time as the time of the message
		setCreateTimestamp( calendar );
		// The identity of the sender is established by the identity of the logged-in user.
		//setCreateUser( IdentityInteraction.getThreadLocalAuthenticatedUser() );
	}

	@PreUpdate
	private void preUpdate() {
		Calendar calendar = Calendar.getInstance();
		// Use the server time as the time of the message
		setUpdateTimestamp( calendar );
		// The identity of the sender is established by the identity of the logged-in user.
		//setUpdateUser( IdentityInteraction.getThreadLocalAuthenticatedUser() );
	}

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public String getProjectDescription() {
		return projectDescription;
	}

	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}

	public String getOwnerEmailId() {
		return ownerEmailId;
	}

	public void setOwnerEmailId(String ownerEmailId) {
		this.ownerEmailId = ownerEmailId;
	}

	public String getOwnerTelephone() {
		return ownerTelephone;
	}

	public void setOwnerTelephone(String ownerTelephone) {
		this.ownerTelephone = ownerTelephone;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Calendar getCreateTimestamp() {
		return createTimestamp;
	}

	public void setCreateTimestamp(Calendar createTimestamp) {
		this.createTimestamp = createTimestamp;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Calendar getUpdateTimestamp() {
		return updateTimestamp;
	}

	public void setUpdateTimestamp(Calendar updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}


}
