package com.mon.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity 
@IdClass(SystemId.class)
@Table(name = "PROJECT_SYSTEMS")
public class System {
	
	/*@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PROJECT_ID", nullable=false)
	private Project project;
	*/
	
	@Id
    @Column(name = "PROJECT_ID")
    private int projectId;
    
	/* Systems ID will be 3 char acronym . e.g.: MDG, WCC etc. */
	@Id
    @Column(name = "SYSTEM_ID", length = 3, nullable=false)
    private String systemId;
    
    @Column(name = "SYSTEM_NAME", length = 50, nullable=false)
    private String systemName;
    
    @Column(name = "INBOUND_ENABLED", nullable=false)
    private Boolean inboundEnabled;
    
    @Column(name = "OUTBOUND_ENABLED", nullable=false)
    private String outboundEnabled;
    
    @Column(name = "CREATE_USER", length = 25, nullable=false)
    private String createUser;

    @Column(name = "CREATE_TIME")
	@Temporal(TemporalType.TIMESTAMP)
	protected Calendar createTimestamp;

    @Column(name = "UPDATE_USER", length = 25, nullable=false)
    private String updateUser;

    @Column(name = "UPDATE_TIME")
	@Temporal(TemporalType.TIMESTAMP)
	protected Calendar updateTimestamp;

	@PrePersist
	private void preInsert() {
		Calendar calendar = Calendar.getInstance();
		// Use the server time as the time of the message
		setCreateTimestamp( calendar );
		// The identity of the sender is established by the identity of the logged-in user.
		//setCreateUser( IdentityInteraction.getThreadLocalAuthenticatedUser() );
	}

	@PreUpdate
	private void preUpdate() {
		Calendar calendar = Calendar.getInstance();
		// Use the server time as the time of the message
		setUpdateTimestamp( calendar );
		// The identity of the sender is established by the identity of the logged-in user.
		//setUpdateUser( IdentityInteraction.getThreadLocalAuthenticatedUser() );
	}

    /*
	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}*/

	public String getSystemId() {
		return systemId;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	public String getSystemName() {
		return systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	public Boolean getInboundEnabled() {
		return inboundEnabled;
	}

	public void setInboundEnabled(Boolean inboundEnabled) {
		this.inboundEnabled = inboundEnabled;
	}

	public String getOutboundEnabled() {
		return outboundEnabled;
	}

	public void setOutboundEnabled(String outboundEnabled) {
		this.outboundEnabled = outboundEnabled;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Calendar getCreateTimestamp() {
		return createTimestamp;
	}

	public void setCreateTimestamp(Calendar createTimestamp) {
		this.createTimestamp = createTimestamp;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Calendar getUpdateTimestamp() {
		return updateTimestamp;
	}

	public void setUpdateTimestamp(Calendar updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}
    
}


