package com.mon.util;

import java.util.HashMap;
import java.util.Map;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.sql.DataSource;

import org.eclipse.persistence.config.PersistenceUnitProperties;
//import org.eclipse.persistence.config.PersistenceUnitProperties;
//import org.eclipse.persistence.jpa.PersistenceProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mon.web.ProjectRegistryServlet;


/**
 * 
 * Utility class for initializing entity manager factory.
 * 
 */
public class EntityManagerUtility {
	
	private static EntityManagerFactory emf;
	private static final Logger LOG = LoggerFactory.getLogger(ProjectRegistryServlet.class);

	public static EntityManagerFactory getEntityManagerFactory() {
		if (emf == null) {
			
			/*try {
	            InitialContext ctx = new InitialContext();
	            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/DefaultDB");

	            Map properties = new HashMap();
	            properties.put(PersistenceUnitProperties.NON_JTA_DATASOURCE, ds);
	            emf = Persistence.createEntityManagerFactory("MessageMonitor", properties);
	            
	            LOG.info("Utility is Called!!!");

	        } catch (NamingException e) {
	        	e.printStackTrace();
	        }
			*/
			throw new IllegalArgumentException(
					"EntityManagerfactory is not initialized!!!");
		}
		return emf;
	}

	public static void setEntityManagerFactory(EntityManagerFactory emf) {
		EntityManagerUtility.emf = emf;
	}
}
