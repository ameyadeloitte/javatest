/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.mon.data;

import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.commons.api.data.Property;
import org.apache.olingo.commons.api.data.ValueType;
import org.apache.olingo.commons.api.edm.EdmEntitySet;
import org.apache.olingo.commons.api.edm.EdmEntityType;
import org.apache.olingo.commons.api.edm.EdmKeyPropertyRef;
import org.apache.olingo.commons.api.edm.EdmPrimitiveTypeKind;
import org.apache.olingo.commons.api.edm.provider.CsdlProperty;
import org.apache.olingo.commons.api.ex.ODataRuntimeException;
import org.apache.olingo.commons.api.http.HttpMethod;
import org.apache.olingo.commons.api.http.HttpStatusCode;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mon.model.Project;
import com.mon.odata.ProjectRegistryEdmProvider;
import com.mon.util.EntityManagerUtility;
import com.mon.util.Util;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

public class Storage {

  private List<Entity> projectList;
  private static final Logger LOGGER = LoggerFactory.getLogger(Storage.class);
  
  public Storage() {
	projectList = new ArrayList<Entity>();
    //initSampleData();
  }

  /* PUBLIC FACADE */

  public EntityCollection readEntitySetData(EdmEntitySet edmEntitySet) throws ODataApplicationException {

    // actually, this is only required if we have more than one Entity Sets
    if (edmEntitySet.getName().equals(ProjectRegistryEdmProvider.ES_PROJECTS)) {
      return getProjects();
    }

    return null;
  }

  public Entity readEntityData(EdmEntitySet edmEntitySet, List<UriParameter> keyParams)
      throws ODataApplicationException {

    EdmEntityType edmEntityType = edmEntitySet.getEntityType();

    // actually, this is only required if we have more than one Entity Type
    if (edmEntityType.getName().equals(ProjectRegistryEdmProvider.ET_PROJECT)) {
      return getProject(edmEntityType, keyParams);
    }

    return null;
  }

  public Entity createEntityData(EdmEntitySet edmEntitySet, Entity entityToCreate) {

    EdmEntityType edmEntityType = edmEntitySet.getEntityType();

    // actually, this is only required if we have more than one Entity Type
    if (edmEntityType.getName().equals(ProjectRegistryEdmProvider.ET_PROJECT)) {
      return createProject(edmEntityType, entityToCreate);
    }

    return null;
  }

  /**
   * This method is invoked for PATCH or PUT requests
   * */
  public void updateEntityData(EdmEntitySet edmEntitySet, List<UriParameter> keyParams, Entity updateEntity,
      HttpMethod httpMethod) throws ODataApplicationException {

    EdmEntityType edmEntityType = edmEntitySet.getEntityType();

    // actually, this is only required if we have more than one Entity Type
    if (edmEntityType.getName().equals(ProjectRegistryEdmProvider.ET_PROJECT)) {
      updateProject(edmEntityType, keyParams, updateEntity, httpMethod);
    }
  }

  public void deleteEntityData(EdmEntitySet edmEntitySet, List<UriParameter> keyParams)
      throws ODataApplicationException {

    EdmEntityType edmEntityType = edmEntitySet.getEntityType();

    // actually, this is only required if we have more than one Entity Type
    if (edmEntityType.getName().equals(ProjectRegistryEdmProvider.ET_PROJECT)) {
      deleteProject(edmEntityType, keyParams);
    }
  }

  /* INTERNAL */

  private EntityCollection getProjects() {
    EntityCollection retEntitySet = new EntityCollection();
    EntityManager em = EntityManagerUtility.getEntityManagerFactory().createEntityManager();
    List<Project> projects;
    
    if(this.projectList == null || this.projectList.size() == 0){
    	// query the Project entity
    	  TypedQuery<Project> query =
    		      em.createNamedQuery("Project.findAll", Project.class);
    	  projects = query.getResultList();
    	  
    	  for (Project project : projects) {
    			Entity e1 = new Entity().addProperty(new Property(null, "ProjectId", ValueType.PRIMITIVE, project.getProjectId()))
    					.addProperty(new Property(null, "ProjectDescription", ValueType.PRIMITIVE, project.getProjectDescription()))
    					.addProperty(new Property(null, "OwnerEmailId", ValueType.PRIMITIVE, project.getOwnerEmailId()))
    					.addProperty(new Property(null, "OwnerTelephone", ValueType.PRIMITIVE, project.getOwnerTelephone()))
    					.addProperty(new Property(null, "CreateUser", ValueType.PRIMITIVE, project.getCreateUser()))
    					.addProperty(new Property(null, "CreateTimestamp", ValueType.PRIMITIVE, project.getCreateTimestamp()))
    					.addProperty(new Property(null, "UpdateUser", ValueType.PRIMITIVE, project.getUpdateUser()))
    					.addProperty(new Property(null, "UpdateTimestamp", ValueType.PRIMITIVE, project.getUpdateTimestamp()));
    			
    			e1.setId(createId("Projects", project.getProjectId()));
    			projectList.add(e1);    		  
    	  }
    }
    
    for (Entity projectEntity : this.projectList) {
      retEntitySet.getEntities().add(projectEntity);
    }

    return retEntitySet;
  }

  private Entity getProject(EdmEntityType edmEntityType, List<UriParameter> keyParams)
					throws ODataApplicationException {

    // the list of entities at runtime
    EntityCollection entitySet = getProjects();

    /* generic approach to find the requested entity */
    Entity requestedEntity = Util.findEntity(edmEntityType, entitySet, keyParams);

    if (requestedEntity == null) {
      // this variable is null if our data doesn't contain an entity for the requested key
      // Throw suitable exception
      throw new ODataApplicationException("Entity for requested key doesn't exist",
          HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.ENGLISH);
    }

    return requestedEntity;
  }

  private Entity createProject(EdmEntityType edmEntityType, Entity entity) {

    // the ID of the newly created product entity is generated automatically
    int newId = 1;
    while (projectIdExists(newId)) {
      newId++;
    }

    Property idProperty = entity.getProperty("ProjectId");
    if (idProperty != null) {
      idProperty.setValue(ValueType.PRIMITIVE, Integer.valueOf(newId));
    } else {
      // as of OData v4 spec, the key property can be omitted from the POST request body
      entity.getProperties().add(new Property(null, "ProjectId", ValueType.PRIMITIVE, newId));
    }
    entity.setId(createId("Project", newId));
    this.projectList.add(entity);
    
    //Persist the new Project
    EntityManager em = EntityManagerUtility.getEntityManagerFactory().createEntityManager();
    try {
    	Project newProject = new Project();
    	newProject.setProjectId(Integer.valueOf(entity.getProperty("ProjectId").getValue().toString()));
    	newProject.setProjectDescription(entity.getProperty("ProjectDescription").getValue().toString());
    	newProject.setOwnerEmailId(entity.getProperty("OwnerEmailId").getValue().toString());
    	newProject.setOwnerTelephone(entity.getProperty("OwnerTelephone").getValue().toString());
    	newProject.setCreateUser(entity.getProperty("CreateUser").getValue().toString());
    	/*newProject.setCreateTimestamp((Calendar)entity.getProperty("CreateTimestamp").getValue());*/
    	newProject.setUpdateUser(entity.getProperty("UpdateUser").getValue().toString());
    	/*newProject.setUpdateTimestamp((Calendar)entity.getProperty("UpdateTimestamp").getValue());
    	*/
    	em.getTransaction().begin();
    	em.persist(newProject);
    	em.getTransaction().commit();
    	
    	LOGGER.info("Information: Project Data Committed");
    } finally {
    	em.close();
    }
    

    return entity;

  }

  private boolean projectIdExists(int id) {

    for (Entity entity : this.projectList) {
      Integer existingID = (Integer) entity.getProperty("ProjectId").getValue();
      if (existingID.intValue() == id) {
        return true;
      }
    }

    return false;
  }

  private void updateProject(EdmEntityType edmEntityType, List<UriParameter> keyParams, Entity entity,
      HttpMethod httpMethod) throws ODataApplicationException {

    Entity projectEntity = getProject(edmEntityType, keyParams);
    if (projectEntity == null) {
      throw new ODataApplicationException("Entity not found", HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.ENGLISH);
    }

    // loop over all properties and replace the values with the values of the given payload
    // Note: ignoring ComplexType, as we don't have it in our odata model
    List<Property> existingProperties = projectEntity.getProperties();
    for (Property existingProp : existingProperties) {
      String propName = existingProp.getName();

      // ignore the key properties, they aren't updateable
      if (isKey(edmEntityType, propName)) {
        continue;
      }

      Property updateProperty = entity.getProperty(propName);
      // the request payload might not consider ALL properties, so it can be null
      if (updateProperty == null) {
        // if a property has NOT been added to the request payload
        // depending on the HttpMethod, our behavior is different
        if (httpMethod.equals(HttpMethod.PATCH)) {
          // as of the OData spec, in case of PATCH, the existing property is not touched
          continue; // do nothing
        } else if (httpMethod.equals(HttpMethod.PUT)) {
          // as of the OData spec, in case of PUT, the existing property is set to null (or to default value)
          existingProp.setValue(existingProp.getValueType(), null);
          continue;
        }
      }

      // change the value of the properties
      existingProp.setValue(existingProp.getValueType(), updateProperty.getValue());
    }
  }

  private void deleteProject(EdmEntityType edmEntityType, List<UriParameter> keyParams)
      throws ODataApplicationException {

    Entity projectEntity = getProject(edmEntityType, keyParams);
    if (projectEntity == null) {
      throw new ODataApplicationException("Entity not found", HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.ENGLISH);
    }

    this.projectList.remove(projectEntity);
  }

  /* HELPER */

  private boolean isKey(EdmEntityType edmEntityType, String propertyName) {
    List<EdmKeyPropertyRef> keyPropertyRefs = edmEntityType.getKeyPropertyRefs();
    for (EdmKeyPropertyRef propRef : keyPropertyRefs) {
      String keyPropertyName = propRef.getName();
      if (keyPropertyName.equals(propertyName)) {
        return true;
      }
    }
    return false;
  }

  private void initSampleData(){

	// add some sample project entities
	final Entity e1 = new Entity().addProperty(new Property(null, "ProjectId", ValueType.PRIMITIVE, 1))
			.addProperty(new Property(null, "ProjectDescription", ValueType.PRIMITIVE, "Global Supplier Management"))
			.addProperty(new Property(null, "OwnerEmailId", ValueType.PRIMITIVE, "jamie.gerber@wal-mart.com"))
			.addProperty(new Property(null, "OwnerTelephone", ValueType.PRIMITIVE, "+19999999999"))
			.addProperty(new Property(null, "CreateUser", ValueType.PRIMITIVE, "TestUser"))
			.addProperty(new Property(null, "CreateTimestamp", ValueType.PRIMITIVE, null))
			.addProperty(new Property(null, "UpdateUser", ValueType.PRIMITIVE, null))
			.addProperty(new Property(null, "UpdateTimestamp", ValueType.PRIMITIVE, null));
	
	e1.setId(createId("Projects", 1));
	projectList.add(e1);
  }

  private URI createId(String entitySetName, Object id) {
    try {
      return new URI(entitySetName + "(" + String.valueOf(id) + ")");
    } catch (URISyntaxException e) {
      throw new ODataRuntimeException("Unable to create id for entity: " + entitySetName, e);
    }
  }
}
