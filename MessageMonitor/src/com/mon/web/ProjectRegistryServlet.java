package com.mon.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.olingo.server.api.OData;
import org.apache.olingo.server.api.ODataHttpHandler;
import org.apache.olingo.server.api.ServiceMetadata;
import org.apache.olingo.server.api.edmx.EdmxReference;
import org.eclipse.persistence.config.PersistenceUnitProperties;
import org.eclipse.persistence.jpa.PersistenceProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mon.data.Storage;
import com.mon.odata.ProjectRegistryEdmProvider;
import com.mon.odata.ProjectRegistryEntityCollectionProcessor;
import com.mon.odata.ProjectRegistryEntityProcessor;
import com.mon.util.EntityManagerUtility;



public class ProjectRegistryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOG = LoggerFactory.getLogger(ProjectRegistryServlet.class);

	@Override
	public void init() throws ServletException {
        try {
            InitialContext ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/DefaultDB");

            Map properties = new HashMap();
            properties.put(PersistenceUnitProperties.NON_JTA_DATASOURCE, ds);
            
            EntityManagerUtility.setEntityManagerFactory(Persistence.createEntityManagerFactory("MessageMonitor", properties));
            
            LOG.info("Servlet Init Called!!!");
  	      // Load seed data
  	      /*if (first_invocation) {
  	    	  	DataLoader m = new DataLoader( emf );
  	    	  	m.loadData();
  	    	  	first_invocation = false;
  	      }*/
        } catch (NamingException e) {
        	e.printStackTrace();
            throw new ServletException(e);
        }
	}

	protected void service(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		try {
	        HttpSession session = req.getSession(true);
	        Storage storage = (Storage) session.getAttribute(Storage.class.getName());
	        if (storage == null) {
	           storage = new Storage();
	           session.setAttribute(Storage.class.getName(), storage);
	        }			
			
	       /* 
	        try {
	            InitialContext ctx = new InitialContext();
	            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/DefaultDB");

	            Map properties = new HashMap();
	            properties.put(PersistenceUnitProperties.NON_JTA_DATASOURCE, ds);
	            EntityManagerUtility.setEntityManagerFactory(new PersistenceProvider().createEntityManagerFactory("MessageMonitorJPA", properties));
	            
	            LOG.info("Servlet Service is Called!!!");

	        } catch (NamingException e) {
	        	e.printStackTrace();
	            throw new ServletException(e);
	        }*/
	        
			// create odata handler and configure it with CsdlEdmProvider and
			// Processor
			OData odata = OData.newInstance();
			ServiceMetadata edm = odata.createServiceMetadata(new ProjectRegistryEdmProvider(), new ArrayList<EdmxReference>());
			ODataHttpHandler handler = odata.createHandler(edm);
			handler.register(new ProjectRegistryEntityCollectionProcessor(storage));
			handler.register(new ProjectRegistryEntityProcessor(storage));

			// let the handler do the work
			handler.process(req, resp);
		} catch (RuntimeException e) {
			LOG.error("Server Error occurred in ProjectRegistryServlet", e);
			throw new ServletException(e);
		}
	}
}
