package com.mon.odata;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.olingo.commons.api.edm.EdmPrimitiveTypeKind;
import org.apache.olingo.commons.api.edm.FullQualifiedName;
import org.apache.olingo.commons.api.edm.provider.CsdlAbstractEdmProvider;
import org.apache.olingo.commons.api.edm.provider.CsdlEntityContainer;
import org.apache.olingo.commons.api.edm.provider.CsdlEntityContainerInfo;
import org.apache.olingo.commons.api.edm.provider.CsdlEntitySet;
import org.apache.olingo.commons.api.edm.provider.CsdlEntityType;
import org.apache.olingo.commons.api.edm.provider.CsdlNavigationProperty;
import org.apache.olingo.commons.api.edm.provider.CsdlProperty;
import org.apache.olingo.commons.api.edm.provider.CsdlPropertyRef;
import org.apache.olingo.commons.api.edm.provider.CsdlSchema;
import org.apache.olingo.commons.api.ex.ODataException;

public class ProjectRegistryEdmProvider extends CsdlAbstractEdmProvider {
	
	
		// Service Namespace
		public static final String NAMESPACE = "OData.MsgMom";

		// EDM Container
		public static final String CONTAINER_NAME = "Container";
		public static final FullQualifiedName CONTAINER = new FullQualifiedName(NAMESPACE, CONTAINER_NAME);

		// Entity Types Names
		public static final String ET_PROJECT = "Project";
		public static final FullQualifiedName ET_PROJECT_FQN = new FullQualifiedName(NAMESPACE, ET_PROJECT);
		
		public static final String ET_SYSTEM = "System";
		public static final FullQualifiedName ET_SYSTEM_FQN = new FullQualifiedName(NAMESPACE, ET_SYSTEM);

		public static final String ET_ERROR = "Error";
		public static final FullQualifiedName ET_ERROR_FQN = new FullQualifiedName(NAMESPACE, ET_ERROR);
		
		// Entity Set Names
		public static final String ES_PROJECTS = "Projects";
		public static final String ES_SYSTEMS  = "Systems";
		public static final String ES_ERRORS   = "Errors";
		
		
		@Override
		public CsdlEntityContainer getEntityContainer() throws ODataException {
			
			  // create EntitySets
			  List<CsdlEntitySet> entitySets = new ArrayList<CsdlEntitySet>();
			  entitySets.add(getEntitySet(CONTAINER, ES_PROJECTS));
			  //entitySets.add(getEntitySet(CONTAINER, ES_SYSTEMS));
			  //entitySets.add(getEntitySet(CONTAINER, ES_ERRORS));
			  
			  // create EntityContainer
			  CsdlEntityContainer entityContainer = new CsdlEntityContainer();
			  entityContainer.setName(CONTAINER_NAME);
			  entityContainer.setEntitySets(entitySets);

			  return entityContainer;
		}

		@Override
		public CsdlEntityContainerInfo getEntityContainerInfo(FullQualifiedName entityContainerName) throws ODataException {
		    // This method is invoked when displaying the Service Document at e.g. http://localhost:8080/MessageMonitorJPA/ProjectRegistry.svc/
		    if (entityContainerName == null || entityContainerName.equals(CONTAINER)) {
		        CsdlEntityContainerInfo entityContainerInfo = new CsdlEntityContainerInfo();
		        entityContainerInfo.setContainerName(CONTAINER);
		        return entityContainerInfo;
		    }

		    return null;
		}
		
		@Override
		public CsdlEntitySet getEntitySet(FullQualifiedName entityContainer, String entitySetName) throws ODataException {
			if (entityContainer.equals(CONTAINER)) {
				if (entitySetName.equals(ES_PROJECTS)) {
					CsdlEntitySet entitySet = new CsdlEntitySet();
					entitySet.setName(ES_PROJECTS);
					entitySet.setType(ET_PROJECT_FQN);

					return entitySet;
				}
			}

			return null;
		}
		
		@Override
		public CsdlEntityType getEntityType(FullQualifiedName entityTypeName) throws ODataException {
			
			//Properties common 
			
			// this method is called for one of the EntityTypes that are configured
			// in the Schema
			if (entityTypeName.equals(ET_PROJECT_FQN)) {

				// create EntityType properties
				CsdlProperty projectId = new CsdlProperty().setName("ProjectId")
						.setType(EdmPrimitiveTypeKind.Int16.getFullQualifiedName());
				CsdlProperty projectDescription = new CsdlProperty().setName("ProjectDescription")
						.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());
				CsdlProperty ownerEmailId = new CsdlProperty().setName("OwnerEmailId")
						.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());
				CsdlProperty ownerTelephone = new CsdlProperty().setName("OwnerTelephone")
						.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());
				CsdlProperty createUser = new CsdlProperty().setName("CreateUser")
						.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());		
				CsdlProperty createTimestamp = new CsdlProperty().setName("CreateTimestamp")
						.setType(EdmPrimitiveTypeKind.Date.getFullQualifiedName());	
				CsdlProperty updateUser = new CsdlProperty().setName("UpdateUser")
						.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());
				CsdlProperty updateTimestamp = new CsdlProperty().setName("UpdateTimestamp")
						.setType(EdmPrimitiveTypeKind.Date.getFullQualifiedName());	
				
				// create CsdlPropertyRef for Key element
				CsdlPropertyRef propertyRef = new CsdlPropertyRef();
				propertyRef.setName("ProjectId");

				// configure EntityType
				CsdlEntityType entityType = new CsdlEntityType();
				entityType.setName(ET_PROJECT);
				entityType.setProperties(Arrays.asList(projectId, projectDescription, ownerEmailId, ownerTelephone, createUser, createTimestamp, updateUser, updateTimestamp));
				entityType.setKey(Collections.singletonList(propertyRef));

				return entityType;
			} else if (entityTypeName.equals(ET_SYSTEM_FQN))  {
				
				// create EntityType properties
				CsdlProperty systemId = new CsdlProperty().setName("SystemId")
						.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());
				CsdlProperty systemName = new CsdlProperty().setName("SystemName")
						.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());
				CsdlProperty inboundEventEnabled = new CsdlProperty().setName("InboundEventEnabled")
						.setType(EdmPrimitiveTypeKind.Boolean.getFullQualifiedName());				
				CsdlProperty outboundEventEnabled = new CsdlProperty().setName("OutboundEventEnabled")
						.setType(EdmPrimitiveTypeKind.Boolean.getFullQualifiedName());
				CsdlProperty createUser = new CsdlProperty().setName("CreateUser")
						.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());		
				CsdlProperty createTimestamp = new CsdlProperty().setName("CreateTimestamp")
						.setType(EdmPrimitiveTypeKind.Date.getFullQualifiedName());	
				CsdlProperty updateUser = new CsdlProperty().setName("UpdateUser")
						.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());
				CsdlProperty updateTimestamp = new CsdlProperty().setName("UpdateTimestamp")
						.setType(EdmPrimitiveTypeKind.Date.getFullQualifiedName());	
				
				// create CsdlPropertyRef for Key element
				CsdlPropertyRef propertyRef = new CsdlPropertyRef();
				propertyRef.setName("SystemId");
				
				// navigation property: many-to-one, null not allowed (product must have a category)
				CsdlNavigationProperty navProp = new CsdlNavigationProperty().setName("Project")
		                                                                   	 .setType(ET_PROJECT_FQN).setNullable(true)
		                                                                     .setPartner("Systems");
				List<CsdlNavigationProperty> navPropList = new ArrayList<CsdlNavigationProperty>();
				navPropList.add(navProp);				
			
				// configure EntityType
				CsdlEntityType entityType = new CsdlEntityType();
				entityType.setName(ET_SYSTEM);
				entityType.setProperties(Arrays.asList(systemId, systemName, inboundEventEnabled, outboundEventEnabled, createUser, createTimestamp, updateUser, updateTimestamp));
				entityType.setKey(Collections.singletonList(propertyRef));
			    entityType.setNavigationProperties(navPropList);
			}

			return null;
		}

		
		//Needed for $metadata
		@Override
		public List<CsdlSchema> getSchemas() throws ODataException {
			  // create Schema
			  CsdlSchema schema = new CsdlSchema();
			  schema.setNamespace(NAMESPACE);

			  // add EntityTypes
			  List<CsdlEntityType> entityTypes = new ArrayList<CsdlEntityType>();
			  entityTypes.add(getEntityType(ET_PROJECT_FQN));
			  schema.setEntityTypes(entityTypes);

			  // add EntityContainer
			  schema.setEntityContainer(getEntityContainer());

			  // finally
			  List<CsdlSchema> schemas = new ArrayList<CsdlSchema>();
			  schemas.add(schema);

			  return schemas;
		}
		
}
