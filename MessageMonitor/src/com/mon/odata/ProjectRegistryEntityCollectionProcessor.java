package com.mon.odata;

import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.apache.olingo.commons.api.data.ContextURL;
import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.commons.api.data.Property;
import org.apache.olingo.commons.api.data.ValueType;
import org.apache.olingo.commons.api.edm.EdmEntitySet;
import org.apache.olingo.commons.api.edm.EdmEntityType;
import org.apache.olingo.commons.api.ex.ODataRuntimeException;
import org.apache.olingo.commons.api.format.ContentType;
import org.apache.olingo.commons.api.http.HttpHeader;
import org.apache.olingo.commons.api.http.HttpStatusCode;
import org.apache.olingo.server.api.OData;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.ODataLibraryException;
import org.apache.olingo.server.api.ODataRequest;
import org.apache.olingo.server.api.ODataResponse;
import org.apache.olingo.server.api.ServiceMetadata;
import org.apache.olingo.server.api.deserializer.DeserializerException;
import org.apache.olingo.server.api.deserializer.DeserializerResult;
import org.apache.olingo.server.api.deserializer.ODataDeserializer;
import org.apache.olingo.server.api.processor.EntityCollectionProcessor;
import org.apache.olingo.server.api.serializer.EntityCollectionSerializerOptions;
import org.apache.olingo.server.api.serializer.EntitySerializerOptions;
import org.apache.olingo.server.api.serializer.ODataSerializer;
import org.apache.olingo.server.api.serializer.SerializerException;
import org.apache.olingo.server.api.serializer.SerializerResult;
import org.apache.olingo.server.api.uri.UriInfo;
import org.apache.olingo.server.api.uri.UriResource;
import org.apache.olingo.server.api.uri.UriResourceEntitySet;

import com.mon.data.Storage;
import com.mon.util.Util;


public class ProjectRegistryEntityCollectionProcessor implements EntityCollectionProcessor {

	private OData odata;
	private ServiceMetadata serviceMetadata;
	private Storage storage;

	public ProjectRegistryEntityCollectionProcessor(Storage storage) {
		this.storage = storage;
	}
	
	public void init(OData odata, ServiceMetadata serviceMetadata) {
		this.odata = odata;
		this.serviceMetadata = serviceMetadata;

	}

	public void readEntityCollection(ODataRequest request, ODataResponse response, UriInfo uriInfo,
			ContentType responseFormat) throws ODataApplicationException, ODataLibraryException {

		// 1st we have retrieve the requested EntitySet from the uriInfo object
		// (representation of the parsed service URI)
		List<UriResource> resourcePaths = uriInfo.getUriResourceParts();
		UriResourceEntitySet uriResourceEntitySet = (UriResourceEntitySet) resourcePaths.get(0); // in
																									// our
																									// example,
																									// the
																									// first
																									// segment
																									// is
																									// the
																									// EntitySet
		EdmEntitySet edmEntitySet = uriResourceEntitySet.getEntitySet();

		// 2nd: fetch the data from backend for this requested EntitySetName
		// it has to be delivered as EntitySet object
		EntityCollection entitySet = storage.readEntitySetData(edmEntitySet); //getData(edmEntitySet);

		// 3rd: create a serializer based on the requested format (json)
		ODataSerializer serializer = odata.createSerializer(responseFormat);

		// 4th: Now serialize the content: transform from the EntitySet object
		// to InputStream
		EdmEntityType edmEntityType = edmEntitySet.getEntityType();
		ContextURL contextUrl = ContextURL.with().entitySet(edmEntitySet).build();

		final String id = request.getRawBaseUri() + "/" + edmEntitySet.getName();
		EntityCollectionSerializerOptions opts = EntityCollectionSerializerOptions.with().id(id).contextURL(contextUrl)
				.build();
		SerializerResult serializerResult = serializer.entityCollection(serviceMetadata, edmEntityType, entitySet,
				opts);
		InputStream serializedContent = serializerResult.getContent();

		// Finally: configure the response object: set the body, headers and
		// status code
		response.setContent(serializedContent);
		response.setStatusCode(HttpStatusCode.OK.getStatusCode());
		response.setHeader(HttpHeader.CONTENT_TYPE, responseFormat.toContentTypeString());
	}

	/*
	public void createEntity(ODataRequest request, ODataResponse response, UriInfo uriInfo,
		     ContentType requestFormat, ContentType responseFormat)
		    throws ODataApplicationException, DeserializerException, SerializerException {
		
		  // 1. Retrieve the entity type from the URI
		  EdmEntitySet edmEntitySet = Util.getEdmEntitySet(uriInfo);
		  EdmEntityType edmEntityType = edmEntitySet.getEntityType();

		  // 2. create the data in backend
		  // 2.1. retrieve the payload from the POST request for the entity to create and deserialize it
		  InputStream requestInputStream = request.getBody();
		  ODataDeserializer deserializer = this.odata.createDeserializer(requestFormat);
		  DeserializerResult result = deserializer.entity(requestInputStream, edmEntityType);
		  Entity requestEntity = result.getEntity();
		  
		  // 2.2 do the creation in backend, which returns the newly created entity
		  Entity createdEntity = storage.createEntityData(edmEntitySet, requestEntity);

		  // 3. serialize the response (we have to return the created entity)
		  ContextURL contextUrl = ContextURL.with().entitySet(edmEntitySet).build();
		    // expand and select currently not supported
		  EntitySerializerOptions options = EntitySerializerOptions.with().contextURL(contextUrl).build();

		  ODataSerializer serializer = this.odata.createSerializer(responseFormat);
		  SerializerResult serializedResponse = serializer.entity(serviceMetadata, edmEntityType, createdEntity, options);

		  //4. configure the response object
		  response.setContent(serializedResponse.getContent());
		  response.setStatusCode(HttpStatusCode.CREATED.getStatusCode());
		  response.setHeader(HttpHeader.CONTENT_TYPE, responseFormat.toContentTypeString());
	}
	*/
	/*
	private EntityCollection getData(EdmEntitySet edmEntitySet) {

		EntityCollection projectsCollection = new EntityCollection();
		// check for which EdmEntitySet the data is requested
		if (ProjectRegistryEdmProvider.ES_PROJECTS.equals(edmEntitySet.getName())) {
			List<Entity> projectList = projectsCollection.getEntities();

			// add some sample product entities
			final Entity e1 = new Entity().addProperty(new Property(null, "ProjectId", ValueType.PRIMITIVE, 1))
					.addProperty(new Property(null, "ProjectDescription", ValueType.PRIMITIVE, "Global Supplier Management"))
					.addProperty(new Property(null, "OwnerEmailId", ValueType.PRIMITIVE, "jamie.gerber@wal-mart.com"))
					.addProperty(new Property(null, "OwnerTelephone", ValueType.PRIMITIVE, "+19999999999"))
					.addProperty(new Property(null, "CreateUser", ValueType.PRIMITIVE, "TestUser"))
					.addProperty(new Property(null, "CreateTimestamp", ValueType.PRIMITIVE, null))
					.addProperty(new Property(null, "UpdateUser", ValueType.PRIMITIVE, null))
					.addProperty(new Property(null, "UpdateTimestamp", ValueType.PRIMITIVE, null));
			
			e1.setId(createId("Projects", 1));
			projectList.add(e1);

		}

		return projectsCollection;
	}
	
	private URI createId(String entitySetName, Object id) {
		try {
			return new URI(entitySetName + "(" + String.valueOf(id) + ")");
		} catch (URISyntaxException e) {
			throw new ODataRuntimeException("Unable to create id for entity: " + entitySetName, e);
		}
	}*/
}
